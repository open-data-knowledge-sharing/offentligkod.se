#!/bin/bash

while getopts u:h FLAG
do
  case "${FLAG}" in
    u)
    URL="${OPTARG}"
    ;;
    h)
    echo "usage: $(basename $0) [-h] [-u] URL" >&2
    exit 0
    ;;
    ?)
    echo "usage: $(basename $0) [-h] [-u] URL" >&2
    exit 1
    ;;
  esac
done

curl -s $URL | jq '.[] | select(.state=="opened")' | jq -sc 'map(select(.labels|contains(["Addera"])))'
