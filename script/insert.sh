#!/bin/bash

while getopts p:d:h FLAG
do
  case "${FLAG}" in
    p)
    GITLAB_PROJECT="${OPTARG}"
    re='^[0-9]+$'
    if ! [[ $GITLAB_PROJECT =~ $re ]] ; then
      echo "error: Gitlab project should be a number" >&2; exit 2
    fi
    ;;
    d)
    DATABASE="${OPTARG}"
    if [ ! -f "$DATABASE" ]; then
      echo "error: Database $DATABASE does not exist."
      exit 1
    fi
    ;;
    h)
    echo "usage: $(basename $0) [-h] [-u] URL" >&2
    exit 0
    ;;
    ?)
    echo "usage: $(basename $0) [-h] [-u] URL" >&2
    exit 1
    ;;
  esac
done

URL="https://gitlab.com/api/v4/projects/$GITLAB_PROJECT/issues"
#NR_PENDING_ISSUES=$(curl -s $URL | jq '.[] | select(.state=="opened")' | jq -s 'map(select(.labels|contains(["Addera"]))) | length')

if [[ ! -x "script/get-issues.sh" ]]; then
  echo "error: Script get-issues is not executable"
  exit 1
fi

PENDING_ISSUES=$(script/get-issues.sh -u $URL)
NR_PENDING_ISSUES=$(echo "$PENDING_ISSUES" | jq 'length')
ISSUES_ID=$(echo "$PENDING_ISSUES" | jq '.[].iid')
echo "$ISSUES_ID" | sed 's/^/#/' | xargs echo "Lagt till en programvara, Close" > commit-message.txt

if [[ $NR_PENDING_ISSUES -eq 0 ]]
then
  echo "No issues to process"
  exit 0
fi

for (( i=0; i<$NR_PENDING_ISSUES; i++ ))
do
  RECORD=$(echo "$PENDING_ISSUES" | jq --arg counter "$i" '.[$counter|tonumber].description' | xargs printf | awk '{ print "-r \""$0"\" \\"}')

  COMMAND="recins -t Programvara \\"
  COMMAND+="${RECORD}\\\n"
  COMMAND+="--verbose $DATABASE"

  eval "$(printf "$COMMAND")"
done
