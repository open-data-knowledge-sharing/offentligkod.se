# Readme

Källkod för webbplatsen (offentligkod.se) och datakällen för alla programvaror.

## Mål
- Enkelt underhåll av datan.
- Kostnadsfri drift.
- Inga servrar
- Möjligt att uppdatera av alla 
- Tillägg av nya programvaror ska vara möjligt av icke-tekniker
- Spåra vem som gjort vad
- Automatiska backuper etc
- Inga inlåsningar till stängda format etc.

# Databas

Egenskaper:

- Databasfil i klartext som kan läses och editeras med ett vanligt textprogram
- Databasformatet är inte kopplat till en specifik programvara.
- Full spårbarhet då filen källkodshanteras med GIT.

[Databasen (programvaror.rec)](data/programvaror.rec)

### Dataschema 
Som DBMS används mjukvaran [RECUTILS](https://www.gnu.org/software/recutils/). (Behövs som sagt inte med kräver mer av den som editerar filen direkt)

Dataschema:
```
%rec: Programvara
%key: Id
%auto: Id
%mandatory: Name UsedBy
%type: UsedBy rec Organisation
%sort: Name
%doc: 
+ En programvara som används av en offentlig organisation.

%rec: Organisation
%key: Id
%auto: Id
```

Två records definieras, Programvara och Organisation. Varje Programvara och Organisation har löpnummer som id som räknas upp automatiskt om DBMS används.
